# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 14:33:59 2021

@author: Maicol
"""

# Set cardinal points dictionary

cardinalPoints = {
    'N': [0,1],
    'E': [1,0],
    'S': [0,-1],
    'W': [-1,0]
}

cardinalPointsList = list(cardinalPoints.keys())


# Set test Scenario 1

testScenarioOne = '''5 5
1 2 N
3 3 E
L M L M L M L M M
M M R M M R M R R M'''


testScenarioTwo = '''3 6
0 2 S
3 3 E
M L M M L M R M
M M L M R M L M'''


crashTesting = '''2 2
0 0 E
1 0 E
M M
M M'''

def prettyPrintTable(table):
    for i in range(0,len(table)):
        string=''
        for j in range(0,len(table[0])):
            string+= table[i][j] + '   '
        
        print(string)
    print('----------------------------------')

def robotMovement(robot, ordersList): 
    for order in ordersList:
        if order == 'L':
            #print("Rotation to the left")
            currentOrientation = cardinalPointsList.index(robot[2])
            newOrientarion = currentOrientation - 1
            if(currentOrientation  < 0):
                newOrientarion= cardinalPointsList[3]
            else:
                newOrientarion= cardinalPointsList[newOrientarion]
            robot[2] = newOrientarion
            robot[1] = cardinalPoints[newOrientarion]   
            
            #print(newOrientarion)
            #print('----------------------------------')
        elif order == 'R':
            #print("rotation to the rigth")
            currentOrientation = cardinalPointsList.index(robot[2])
            #print("index", currentOrientation)
            newOrientarion = currentOrientation + 1
            if(newOrientarion > 3):
                newOrientarion= cardinalPointsList[0]
            else:
                newOrientarion= cardinalPointsList[newOrientarion]
            
            robot[2] = newOrientarion
            robot[1] = cardinalPoints[newOrientarion]
            #print(newOrientarion)
            #print('----------------------------------')
        elif order == 'M':
            #print("Move forward")

            currentPosition = robot[0]
            #print("currentPosition",currentPosition)
            currentOrientation = robot[1]
            #print("currentOrientation",currentOrientation)

            newPosition = [currentPosition[0] + currentOrientation[0],currentPosition[1] + currentOrientation[1]]
            

            # Prevent fallings

            if newPosition[0] > tableLength:
                newPosition[0] = tableLength
                #print("falling prevention")
                continue
            elif newPosition[0] < 0:
                newPosition[0] = 0
                #print("falling prevention")
                continue
                
            if newPosition[1] > tableHeight:
                newPosition[1] = tableHeight
                #print("falling prevention")
                continue
            elif newPosition[1] < 0:
                newPosition[1] = 0
                #print("falling prevention")
                continue

            # Prevent crashes with other robots

            if table [tableHeight - newPosition[1]] [newPosition[0]] == 'X':
                robot[0] = newPosition
                table [tableHeight - robot[0][1]] [robot[0][0]] = robot[3] + robot[2]
                table [tableHeight - currentPosition[1]][currentPosition[0]] = 'X'
            else:
                print("crash prevention")
            #print("newPosition",newPosition)

            
            #print(' ')
            #prettyPrintTable(table)

    print(robot[0][0],robot[0][1], robot[2])


# Get text lines into a list by splitting it with \n character
# Uncomment line 132 if you want to run with test scenario #2

ordersList = testScenarioOne.split('\n')  
#ordersList = testScenarioTwo.split('\n') 

ordersList = [i.split(' ') for i in ordersList]


# Set table (matrix) variable

tableLength = int(ordersList[0][0]) 
tableHeight = int(ordersList[0][1])
table = [['X']*(tableLength+1) for i in range( int(tableHeight+ 1 )) ]


# Parse Robots positio and locate them into the table

# RN = [[Xposition,Yposition],[Xaxis_Orientation,Yasis_Orientation], robotName]

R1 = [[int(i) for i in ordersList[1][0:2]],cardinalPoints[ordersList[1][2]],ordersList[1][2], 'R1']
R2 = [[int(i) for i in ordersList[2][0:2]],cardinalPoints[ordersList[2][2]],ordersList[2][2], 'R2']


# Locate robots on Table

table[tableHeight - R1[0][1]] [R1[0][0]] = 'R1' + R1[2]
table[tableHeight - R2[0][1]] [R2[0][0]] = 'R2' + R2[2]



# Robot movement

robotMovement(R1,ordersList[3])
robotMovement(R2,ordersList[4])


